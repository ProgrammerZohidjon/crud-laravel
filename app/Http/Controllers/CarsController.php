<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use Image;
use App\Car;

class CarsController extends Controller
{
   
    public function index(){
        return View::make('index',[
            'cars'=>Car::where('status',1)->orderBy('created_at','desc')->paginate(10)
        ])->with('title','Cписок машин');
    }

    
    public function create(){
        return View::make('blocks.create')->with('title','Добавление машин');
    }

    public function store(Request $request){
        $car = new Car;
        $request->validate([
            'name'=>'required','manufacturer'=>'required','model'=>'required','year'=>'required','energy'=>'required','transmission'=>'required','power_motor'=>'required',
            'file'=>'image|mimes:jpeg,png,jpg|max:4096'
        ]);
        $path_img = 'assets/images/cars/';
        $image = $request->file('file');
        if ($image) {
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path($path_img . $filename);
            $img = Image::make($image->getRealPath());
            $img->resize(400,400, function($constrainat){
                $constrainat->aspectRatio();
            })->save($path);
            if($car->image) file_exists($car->image)??unlink(public_path($car->image));
            $car->image = $path_img.$filename;
        }

        $car->name = $request->name;
        $car->manufacturer = $request->manufacturer;
        $car->model = $request->model;
        $car->year = $request->year;
        $car->energy = $request->energy;
        $car->transmission = $request->transmission;
        $car->power_motor = $request->power_motor;
        $car->slug = $request->slug;
        $car->save();
        return redirect()->route('welcome')->with('success','Данные успешно добавлено!');
    }

    
    public function show(Request $request){
        if(isset($request->slug)){
            $car = Car::getCarSlug($request->slug);
            return View::make('blocks.show',compact('car'))->with('title','О машине');
        }else{
            abort(404);
        }
    }

    public function edit(Request $request){
        if (isset($request->id)) {
            $car = Car::findOrFail($request->id);
            if (count($car)) {
                return View::make('blocks.edit',compact('car'))->with('title','Редактирование машины');
            }else{
                return redirect()->back()->with('info','Ошибка при редактировании');
            }
        }else{
            abort(404);
        }
    }

    public function update(Request $request){
        if(isset($request->id)){
            $car = Car::findOrFail($request->id);
            $request->validate([
                'name'=>'required','manufacturer'=>'required','model'=>'required','year'=>'required','energy'=>'required','transmission'=>'required','power_motor'=>'required'
            ]);
            if (count($car)) {
                $path_img = 'assets/images/cars/';
                $image = $request->file('file');
                if ($image) {
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    $path = public_path($path_img . $filename);
                    $img = Image::make($image->getRealPath());
                    $img->resize(400,400, function($constrainat){
                        $constrainat->aspectRatio();
                    })->save($path);
                    if($car->image) !file_exists($car->image)?unlink(public_path($car->image)):'Not image';
                    $car->image = $path_img.$filename;
                }
                $car->name = $request->name;
                $car->manufacturer = $request->manufacturer;
                $car->model = $request->model;
                $car->year = $request->year;
                $car->energy = $request->energy;
                $car->transmission = $request->transmission;
                $car->power_motor = $request->power_motor;
                $car->save();
                return redirect()->route('welcome')->with('success','Данные успешно измененно!');
            }else{
                return redirect()->back()->with('info','Ошибка при редактировании!');
            }
        }
    }

    public function destroy(Request $request){
        if (isset($request->id)) {
            $car = Car::findOrFail($request->id);
            if ($car->image !=null) {
                unlink(public_path().'/'.$car->image);
            }
            $car->delete();
            return back()->with('success', 'Успешно удалено!');
        }
    }
}
