<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Car extends Model
{

	protected $table = 'cars';
	
	protected $fillable = ['name','manufacturer','model','year','energy','transmission','power_motor','status','slug','image'];

	public function setSlugAttribute($value) {
		$this->attributes['slug'] = Str::slug( mb_substr($this->name, 0, 10) . "-" . \Carbon\Carbon::now()->format('dmyHi'), '-');
	}

	public static function getCarSlug($slug){
		return Car::where('status',1)->where('slug',$slug)->first();
	}

}
