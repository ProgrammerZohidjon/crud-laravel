<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','CarsController@index')->name('welcome');
Route::get('/car/create','CarsController@create')->name('car.create');
Route::post('/store','CarsController@store')->name('car.store');
Route::get('/car/edit/{id}','CarsController@edit')->name('car.edit');
Route::post('/update','CarsController@update')->name('car.update');
Route::get('/car/show/{slug}','CarsController@show')->name('car.show');
Route::get('/car/delete/{id}','CarsController@destroy')->name('car.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
