<h1>{{$title}}</h1>
<ol class="breadcrumb">
	<li><a href="{{route('welcome')}}">{{$parent}}</a></li>
	<li class="active">{{$active}}</li>
</ol>