@extends('layouts.layouts')

@section('breadcrumb')
@component('components.breadcrumb')
@slot('title') Список Машин @endslot
@slot('parent') Главная @endslot
@slot('active') Список Машин @endslot
@endcomponent
@endsection

@section('content')
<div class="row">
  <div class="alert alert-success" style="display:none"></div>
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><a type="button" class="btn btn-block btn-primary btn-sm" href="{{route('car.create')}}">Добавить</a></h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <thead>
            <tr>             
              <th>Название</th>
              <th>Производитель</th>
              <th>Модель</th>
              <th>Год выпуска</th>
              <th>Лошадиные силы</th>
              <th>Коробка передач</th>
              <th>Мощность двигателя</th>
              <th style="width: 100px">Действие</th>
            </tr>
          </thead>
          <tbody>
            <?php $n = 1; ?>
            @forelse($cars as $car)
            <tr>
              <td>{{$car->name}}</td>
              <td>{{$car->manufacturer}}</td>
              <td>{{$car->model}}</td>
              <td>{{$car->year}}</td>
              <td>{{$car->energy}}</td>
              <td>{{$car->transmission}}</td>
              <td>{{$car->power_motor}}</td>
              <td>
                <a type="button" class="btn btn-xs btn-default" href="{{route('car.show',$car->slug)}}"><i class="fa fa-eye"></i>
                </a>
                <a type="button" class="btn btn-xs btn-default" href="{{route('car.edit',$car->id)}}"><i class="fa fa-pencil"></i>
                </a>
                <a onclick="if(confirm('Вы действительно хотите Удалить?')){return true}else{return false}" href="{{route('car.destroy',$car->id)}}"  type="button" class="btn btn-xs btn-default"><i class="fa fa-trash"></i>
                </a>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="8" style="text-align: center;">Данные отсутствует</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          {{$cars->links()}}
        </ul>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection