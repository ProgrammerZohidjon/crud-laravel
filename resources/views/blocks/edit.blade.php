@extends('layouts.layouts')

@section('breadcrumb')
@component('components.breadcrumb')
@slot('title') Редактирование машины @endslot
@slot('parent') Главная @endslot
@slot('active') Редактирование машины @endslot
@endcomponent
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Редактирование машины</div>
			<div class="panel-wrapper collapse in" aria-expanded="true">
				<div class="panel-body">
					<form method="post" action="{{route('car.update')}}" enctype="multipart/form-data">
						@csrf
						@include('blocks.form')
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection