@if (count($errors) > 0)
<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<div class="form-body">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">Название:</label>
				<input type="text" class="form-control" name="name" required="" value="{{isset($car['name'])?$car['name']:old('name')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="type">Производитель:</label>
				<input type="text" class="form-control" name="manufacturer" required="" value="{{isset($car['manufacturer'])?$car['manufacturer']:old('manufacturer')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="type">Модель:</label>
				<input type="text" class="form-control" name="model" required="" value="{{isset($car['model'])?$car['model']:old('model')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="type">Год выпуска:</label>
				<input type="text" class="form-control" name="year" required="" value="{{isset($car['year'])?$car['year']:old('year')}}" autocomplete="off">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="type">Лошадиные силы:</label>
				<input type="text" class="form-control" name="energy" required="" value="{{isset($car['energy'])?$car['energy']:old('energy')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="type">Коробка передач:</label>
				<input type="text" class="form-control" name="transmission" required="" value="{{isset($car['transmission'])?$car['transmission']:old('transmission')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="price">Мощность двигателя:</label>
				<input type="text" class="form-control" name="power_motor" value="{{isset($car['power_motor'])?$car['power_motor']:old('power_motor')}}" autocomplete="off">
			</div>
			<div class="form-group">
				<label for="price">Изображение:</label>
				<input type="file" class="form-control" name="file" accept=".jpeg,.png,.jpg">
			</div>
			<input type="hidden" name="id" value="{{isset($car['id'])?$car['id']:old('id')}}">
			@if(isset($car))
				<img src="{{asset($car->image)}}" class="mailbox-attachment-icon has-img">
				@endif
		</div>
		<div class="box-footer">
			<a type="submit" href="{{URL::previous()}}" class="btn btn-default">Отмена</a>
			<button type="submit" class="btn btn-info pull-right">Сохранить</button>
		</div>
	</div>
</div>