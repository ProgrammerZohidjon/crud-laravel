@extends('layouts.layouts')

@section('breadcrumb')
@component('components.breadcrumb')
@slot('title') О машине @endslot
@slot('parent') Главная @endslot
@slot('active') О машине @endslot
@endcomponent
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <img class="img-responsive" src="{{asset($car->image)}}">

        <h3 class="text-center">{{$car->name}}</h3>
        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Название: {{$car->name}}</b>
          </li>
          <li class="list-group-item">
            <b>Производитель: {{$car->manufacturer}}</b>
          </li>
          <li class="list-group-item">
            <b>Модель: {{$car->model}}</b>
          </li>
           <li class="list-group-item">
            <b>Год выпуска: {{$car->year}}</b>
          </li>
          <li class="list-group-item">
            <b>Лошадиные силы: {{$car->energy}}</b>
          </li>
          <li class="list-group-item">
            <b>Коробка передач: {{$car->transmission}}</b>
          </li>
           <li class="list-group-item">
            <b>Мощность двигателя: {{$car->power_motor}}</b>
          </li>
        </ul>

        <a href="{{URL::previous()}}" class="btn btn-default">Назад</a>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection