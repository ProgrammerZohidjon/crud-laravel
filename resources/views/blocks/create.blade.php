@extends('layouts.layouts')

@section('breadcrumb')
@component('components.breadcrumb')
@slot('title') Добавление новой машины @endslot
@slot('parent') Главная @endslot
@slot('active') Добавление новой машины @endslot
@endcomponent
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading"> Добавление новой машины</div>
			<div class="panel-wrapper collapse in" aria-expanded="true">
				<div class="panel-body">
					<form method="post" action="{{route('car.store')}}" enctype="multipart/form-data">
						@csrf
						@include('blocks.form')
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection