**Installation**

1. git clone https://jzshdev@bitbucket.org/ProgrammerZohidjon/crud-laravel.git.
2. Open the terminal and write this command composer install.
3. Rename or copy .env.example file to .env and write required database information.
4. Run php artisan key:generate command.
5. With php artisan migrate command, create the tables.
6. Install packages composer require intervention/image


---

## License

CRUD-Laravel is open-sourced software licensed under the MIT license.